This project's issue tracker has been disabled, if you wish to [create an issue or bug please follow these directions](/CONTRIBUTING.md#issue-tracker).

# GitLab OSS Review Toolkit analyzer

This analyzer is based on [ORT](https://github.com/oss-review-toolkit/ort).
It currently hosts the ORT Docker image in the container registry of this project.
